package com.example.apptest.model;

public class Data {
    private String id, name, email, nomor;

    public Data() {
    }

    public Data(String id, String name, String email, String nomor) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.nomor = nomor;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNomor() {
        return nomor;
    }

    public void setNomor(String nomor) {
        this.nomor = nomor;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
